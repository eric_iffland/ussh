<?php /* 
* Template Name: Custom Accordion
*/ ?>

<?php get_header(); ?>

            <style>

                /*
            PROGRAMI I TRANSPARENCES
            */
            .wf-container-main {display:block !important;}
            
            #pt				{ width: 100%; height: auto; margin: 0 auto 0 auto; overflow: auto; padding: 0 0 30px 0; background-color: #ffffff; box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2 ); }
            #pt h1			{ display: block; margin: 0 0 30px 0; padding: 20px; font-size: 16px; color: #ffffff; font-family: Helvetica, Arial; background-color: #15a161; text-transform: uppercase; border-bottom: 10px solid #15a161; }
            #pt h2			{ display: block; font-size: 14px; color: #202020; font-weight: bold; font-family: Helvetica, Arial; text-transform: uppercase; border-bottom: 0px solid #15a161; }
            
            
            #pt .left			{ display: block; float: none; width: 100%; height: 40px; margin : 0 0 20px 0; padding: 0 0 0 0; background-color: #f0f0f0; overflow: auto; }
            #pt .left a			{ float: left; padding: 0 20px 0 20px; font-size: 14px; color: #888888; font-weight: bold; line-height: 40px; }
            #pt .left .sel		{ color: #aa0000; }
            
            #pt .content 		{ width: 960px; margin: 0 auto 0 auto; line-height: 150%; }
            
            #pt .right			{ display: block; float: none; width: 960px; min-height: 500px; height: auto; font-size: 16px; margin : 0 auto 0 auto; padding: 0 0 20px 0; border-right: 0px dotted #d0d0d0; }
            #pt .right:after		{ content: ""; display: block; height: 0; clear: both; visibility: hidden; }
            #pt .right .imgla		{ float: left; margin: 0 20px 20px 0; }
            #pt .right .imgra		{ float: right; margin: 0 0 20px 20px; }
            #pt .right .imgca		{ display: block; max-width: 100%; margin: 0 auto 20px auto; clear: both; text-align: center; }
            #pt .right .imgf		{ display: block; width: 100%; height: auto; margin: 0 0 20px 0; clear: both; }
            #pt .right p			{ font-size: 14px; margin: 0 0 5px 0; line-height: 150%; }
            #pt .right pre		{ font-size: 14px; margin: 0 0 20px 0; line-height: 150%; }
            #pt .right h1			{ font-size: 24px; margin: 0 0 20px 0; line-height: 150%; }
            #pt .right h2			{ font-size: 20px; margin: 0 0 20px 0; line-height: 150%; }
            #pt .right h3			{ font-size: 16px; margin: 0 0 20px 0; line-height: 150%; }
            #pt .right h4			{ font-size: 14px; margin: 0 0 20px 0; line-height: 150%; }
            #pt .right h5			{ font-size: 12px; margin: 0 0 20px 0; line-height: 150%; }
            #pt .right h6			{ font-size: 8px; margin: 0 0 20px 0; line-height: 150%; }
            #pt .right blockquote		{ margin: 10px 0 20px 50px; padding: 0 0 0 15px; font-size: 16px; border-left: 3px solid #cccccc; line-height: 150%; }
            /*#pt .right ul			{ margin: 0 0 1em 0; padding: 0 0 0 1em; }
            #pt .right ul li		{ margin: 0 0 1em 0; }
            #pt .right ol li		{ margin: 0 0 1em 0; }
            #pt .right ol			{ margin: 0 0 1em 0; padding: 0 0 0 1em; }
            */
            #pt .right hr			{ clear: both; width: 100%; height: 1px; color: #cccccc; border: 0; background-color: #cccccc; }
            
            
            /*#pt table, th, td {
                border: 1px solid gray;
            }
            */
            #pt td { padding: 10px !important; }
            
            
            
            #pt .titleCol {
                width: 450px;
                height: 95px;
                padding: 0 10px 0 10px;
                text-align:center;
                background-color: rgb(46, 104, 170);
                color:#fff;
                font-family: "Arial", "Times New Roman" ;
                font-size: 90%;
                border-radius: 15px;
                vertical-align: middle;
              }
            
            #pt table {
                    border-color: gray; 
                    /*border-collapse: collapse;*/ 
                    border-spacing: 10px 10px;
                    border-collapse: separate;
                    margin-left: auto; 
                    margin-right: auto;
                   
                    /* border-spacing: 20px 0; */
                }
            
            
            #pt ul {
                    list-style: none; /* Remove HTML bullets */
                    padding: 8px;
                    margin: 5px;
                }
            
            #pt p {
                    padding: 5px;
                    margin: 5px;
                }
            
            #pt li {
                    padding:5px 0 5px 16px;
                }
            
            #pt li::before {
                    content: "•"; /* Insert content that looks like bullets */
                    padding-right: 8px;
                    color: #045e7a; /* Or a color you prefer */
                }
            
            
            
            #pt ol li{
                list-style:none;
              }
            
            #pt ol li::before{
                content: none; /* Insert content that looks like bullets */
                padding-right: 8px;
                color: #045e7a; /* Or a color you prefer */
              }
            
            #pt td:hover{
                cursor:pointer;
                background-color: #03aadf; /*   #045E7A; */
              }
            
            #pt tr{
                border: 0px;
                border-color: #dcf2e8;
                border-style: solid;
              }
            
            #pt .myContentsDiv{
            
                width: 100% !important;
                padding: 25px;
                display: none;
                font-family: "Arial", "Times New Roman" ;
                border-style: solid;
                border-width: 1px;
                border-color: #2e68aa; /* #ce161c; */
                border-radius: 15px;
               /* background-color: #F7E3E3;  */
              }
            
            #pt .hyperLink{
                color: #0000EE;
                text-decoration: underline;
              }
              
            #pt .hyperLink:hover{
                cursor:pointer;
              }
               </style>
            
                 <script>
            
            
                
                /*
                FOR Programi i Transarencces
                */
                
                var coll = document.getElementsByClassName("collapsible");
                var i;
            
                for (i = 0; i < coll.length; i++) {
                coll[i].addEventListener("click", function() {
                  this.classList.toggle("active");
                  var content = this.nextElementSibling;
                  if (content.style.maxHeight){
                    content.style.maxHeight = null;
                    } else {
                    content.style.maxHeight = content.scrollHeight + "px";
                    }
                    });
                }
            
                function myFunction(divName) {
                var x = document.getElementById(divName);
                if (x.style.display === "none") {
                  x.style.display = "block";
                    } else {
                  x.style.display = "none";
                    }
            
                var childDivs = document.getElementsByTagName('div');
                for( i=0; i< childDivs.length; i++ )
                {
                    var childDiv = childDivs[i];
                    if (childDiv.id.includes("contentDiv") && childDiv.id != x.id)
                        {
                        childDiv.style.display = "none";
                        }
                    }
                }
            
                function hideDivs(){
                var childDivs = document.getElementsByTagName('div');
                    for( i=0; i< childDivs.length; i++ )
                    {
                     var childDiv = childDivs[i];
                    if (childDiv.id.includes("contentDiv"))
                        {
                        childDiv.style.display = "none";
                        }
                    }
                }
                 </script>



<?php /* start page */ ?>

							<div id="pt">
								<div class="right">
									<div class="content">

                    <!-- display only the first item here -->
										<table>
											<tbody>
                        <?php
                        if( have_rows('topics') ):
                          // Initialize a counter variable
                          $counter = 0;
                          // Loop through the rows of data
                          while ( have_rows('topics') ) : the_row();
                            // Increment the counter
                            $counter++;
                            
                            // For the first row, fetch and display data separately
                            if($counter == 1) {
                              // Fetch data from the first row
                              $topic_title = get_sub_field('topic_title');
                              // $topic_content = get_sub_field('topic_content');
                        ?>
												<tr>
												    <td style="width: 315px; background-color: #fff;"></td>
                            <?php echo '<td onclick="myFunction(\'contentDiv ' . $counter . '\')" class="titleCol" style="width: 315px;">'; ?>
                                <p><? echo $topic_title; ?></p>
													  </td>
													  <td style="width: 315px; background-color: #fff;"></td>
												</tr>
                        <?php } ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
											</tbody>
										</table>
									</div>

								<br>

                <div>
                    <!-- display the remaining here -->
                    <table>
										    <tbody>
                        <?php if( have_rows('topics') ):
                          // Initialize a counter variable
                          $counter = 0;
                          // Loop through the rows of data
                          //while ( have_rows('topics') ) : the_row();
                          while ( have_rows('topics') ) : the_row();
                          $counter++;
                          if ($counter != 1) :

                            // Increment the counter
                              $topic_title = get_sub_field('topic_title');
                              // $topic_content = get_sub_field('topic_content');
                            if(($counter - 2) % 3 == 0) { ?>
											      <tr>
                            <?php } ?>
                                <?php echo '<td style="width:33.33%" onclick="myFunction(\'contentDiv ' . $counter . '\')" class="titleCol">'; ?>
                                    <p><? echo $topic_title; ?></p>
                                </td>
                            <?php if(($counter - 2) % 3 == 2) { ?>
                            </tr>
                            <?php } ?>
                        <?php endif; ?>
                        <?php endwhile; ?>
                        <!-- /* every third breaks */ -->
                        <?php endif; ?>

                        </tbody>
                    </table>
                <br>
                </div>



                <!-- display the content here -->

                <?php
                if( have_rows('topics') ):
                  // Initialize a counter variable
                  $counter = 0;
                  // Loop through the rows of data
                  while ( have_rows('topics') ) : the_row();
                    // Increment the counter
                    $counter++;
                    $topic_content = get_sub_field('topic_content');
                ?>

                <?php echo '<div class="myContentsDiv" id="contentDiv ' . $counter . '" class="titleCol" style="margin: auto; display: none;">'; ?>
                    <?php echo $topic_content; ?>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
                


                <div style="background: #fafafa; width: 1000px; margin: auto; border-radius: 15px; border-color: #3a536e; border-width: 2px; border-style: solid;">
                  <table>
                    <tbody><tr>
                      <td class="unChangedHoverStyle"><img class="unChangedHoverStyle" style="width:48px; height: 48px;" src="https://www.ussh.org.al/book.png"></td>
                      <td class="unChangedHoverStyle"><p style="text-align:center; color: #2c313c; font-family:Arial; font-weight: bold;"><a id="orderLink" onmouseover="this.style.color='#03aadf'" onmouseout="this.style.color='#3a536e'" class="unChangedHoverStyle" style="color: rgb(58, 83, 110);" href="https://www.idp.al/wp-content/uploads/2021/03/Urdhri_i-miratimit_te_PT_2021.pdf">Programi i Transparencës së KDIMDP miratuar me Urdhrin Nr. 32, datë 24.03.2021</a></p></td>
                    </tr>
                  </tbody></table>
                
                </div>


            
            
            
		
	</div>
	</div><!-- #content -->
</div>

<? get_footer(); ?>
